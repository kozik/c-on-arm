/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2018 Rafal Kozik
 * All rights reserved.
 */
 
#include <stdint.h>
#include <stdio.h>
#include "stm32f042x6.h"

#define AF 0x02
#define TX 2
#define RX 15
#define SWDIO 13
#define SWCLK 14

int main(void)
{
    RCC->AHBENR = RCC_AHBENR_GPIOAEN;
    RCC->APB2ENR = RCC_APB2ENR_USART1EN;
    
    GPIOA->MODER = (AF << (TX * 2)) |
				   (AF << (RX * 2)) |
				   (AF << (SWDIO * 2)) |
				   (AF << (SWCLK * 2));
    GPIOA->AFR[0] = 1 << (TX * 4);
    GPIOA->AFR[1] = 1 << ((RX - 8) * 4);
    
    USART1->BRR = 8000000 / 115200;
    USART1->CR1 = USART_CR1_UE | USART_CR1_RE |
				  USART_CR1_TE;
	
	printf("Hello world!\r\n");
	for(;;) {
		int n, r;
		printf("Please give a number:\r\n");
		r = scanf("%d", &n);
		if (r == 0) {
			scanf("%*c");
			printf("It is not a number.\r\n");
		} else {
			printf("%d in hex: 0x%x\r\n", n, n);
		}
	}
}

int _write(__attribute__ ((unused)) int file, char *ptr, int len)
{
    for (int i = 0; i < len; i++) {
        while (!(USART1->ISR & USART_ISR_TC));
        USART1->TDR = ptr[i];
    }
	return len;
}

int _read(__attribute__ ((unused)) int file, char *ptr,
		  __attribute__ ((unused)) int len)
{
	while(!(USART1->ISR & USART_ISR_RXNE));
	ptr[0] = USART1->RDR;
	return 1;
}

int _close(__attribute__ ((unused)) int file)
{
	return -1;
}

#include <sys/stat.h>
int _fstat(__attribute__ ((unused)) int file, struct stat *st)
{
	st->st_mode = S_IFCHR;
	return 0;
}

int _isatty(__attribute__ ((unused)) int file)
{
	return 1;
}

int _lseek(__attribute__ ((unused)) int file,
		   __attribute__ ((unused)) int ptr,
		   __attribute__ ((unused)) int dir)
{
	return 0;
}

caddr_t _sbrk(int incr)
{
	extern char _end; /* Defined by the linker */
	static char *heap_end;
	char *prev_heap_end;

	if (heap_end == 0) {
		heap_end = &_end;
	}
	prev_heap_end = heap_end;
	heap_end += incr;
	return (caddr_t) prev_heap_end;
}
