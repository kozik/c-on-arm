/*-
 * SPDX-License-Identifier: BSD-3-Clause
 *
 * Copyright (c) 2018 Rafal Kozik
 * All rights reserved.
 */
#include <stdint.h>

#define IOPBEN (1<<18)
#define LED_PIN 3

struct GPIO {
	volatile uint32_t MODER;
	volatile uint32_t OTYPER;
	volatile uint32_t OSPEEDR;
	volatile uint32_t PUPDR;
	volatile uint32_t IDR;
	volatile uint32_t ODR;
	volatile uint32_t BSRR;
	volatile uint32_t LCKR;
	volatile uint32_t AFRL;
	volatile uint32_t AFRH;
	volatile uint32_t BRR;
};

volatile uint32_t * const RCC_AHBENR = (uint32_t*) 0x40021014;
struct GPIO * const GPIOB = (struct GPIO*)0x48000400;

uint32_t global_a = 35;
uint32_t global_b;

void delay(uint32_t v)
{
	
	for(;v > 0; v--);
}

int main(void)
{

	*RCC_AHBENR = IOPBEN;	
	GPIOB->MODER = 1 << (2*LED_PIN);
	
	for(;;) {
		GPIOB->ODR = ~GPIOB->ODR;
		delay(100000);
	}
}
